package heptathlon.view;

import heptathlon.POJO.Facture;
import heptathlon.POJO.Produit;
import heptathlon.operations.OperationClient;
import heptathlon.operations.OperationService;
import heptathlon.wrapper.ProduitList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class NouvelleFacture extends JFrame {
    private final int width = 300;
    private final int height = 700;
    JTextField nombreSelect;
    JComboBox comboBoxStock;
    JComboBox comboBoxModePaiement;
    JLabel total;

    JTable table;
    DefaultTableModel tableModel;
    JScrollPane scrollPane;

    public NouvelleFacture() {

        OperationService client = OperationClient.getInstance().getClient();
        ProduitList listProduits = new ProduitList();
        try {
            String allProduit = client.getAllProduit();
            JAXBContext jaxbContext = JAXBContext.newInstance(ProduitList.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            listProduits = (ProduitList) jaxbUnmarshaller.unmarshal(new StringReader(allProduit));
            System.out.println(allProduit);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        Produit[] ProduitsBidons = listProduits.asArray();

        // creating instance of JButton
        JLabel produit = new JLabel("Produit :");
        comboBoxStock = new JComboBox(ProduitsBidons);
        comboBoxModePaiement = new JComboBox(Facture.ModePaiement.values());
        JLabel nombre = new JLabel("Qté :");
        total = new JLabel("Total : 0,00 €");
        nombreSelect = new JTextField("1");
        JButton ajouter = new JButton("➕ Ajouter");
        JButton annuler = new JButton("❌ Annuler");
        JButton valider = new JButton("✅ Valider");

        ArrayList<JComponent> composants = new ArrayList<>();
        composants.add(produit);
        composants.add(comboBoxStock);
        composants.add(nombre);
        composants.add(nombreSelect);
        composants.add(annuler);
        composants.add(total);
        composants.add(comboBoxModePaiement);
        composants.add(ajouter);
        composants.add(valider);

        // add button to JFrame
        for (JComponent component : composants) {
            component.setBorder(null);
            component.setBackground(Color.GRAY);
            component.setForeground(Color.WHITE);
            this.add(component);
        }


        tableModel = new DefaultTableModel();
        table = new JTable(tableModel);
        table.setFillsViewportHeight(true);
        tableModel.addColumn("Produit");
        tableModel.addColumn("Prix");
        tableModel.addColumn("Quantité");
        scrollPane = new JScrollPane(table);
        table.setBackground(Color.darkGray);
        table.setForeground(Color.white);
        table.getTableHeader().setBackground(Color.darkGray);
        table.getTableHeader().setForeground(Color.white);
        this.add(scrollPane);
        // Exemple d'usage : tableModel.addRow(row);

        // x axis, y axis, width, height;
        int margin = 15;
        int contentWidth = width - margin * 3;
        int thickness = 40;
        produit.setBounds(margin, margin, contentWidth, thickness);
        comboBoxStock.setBounds(margin, margin + thickness, contentWidth, thickness);
        nombre.setBounds(margin, margin * 3 + thickness * 2, width / 2, thickness);
        nombreSelect.setBounds(width / 2 + margin, margin * 3 + thickness * 2, (width / 2) - margin * 3, thickness);
        ajouter.setBounds(margin, margin * 4 + thickness * 3, contentWidth, thickness);
        scrollPane.setBounds(margin, margin * 5 + thickness * 4, contentWidth, this.height - (margin + thickness * 10));
        total.setBounds(margin, this.height - (thickness * 3 + margin * 2), (width / 2) - (margin * 2), thickness);
        comboBoxModePaiement.setBounds((width / 2), this.height - (thickness * 3 + margin * 2), (width / 2) - (margin * 2), thickness);
        annuler.setBounds(margin, this.height - (thickness * 2 + margin), contentWidth / 2, thickness);
        valider.setBounds(margin + (contentWidth / 2), this.height - (thickness * 2 + margin), contentWidth / 2, thickness);

        valider.setBackground(new Color(18, 80, 38));

        this.setSize(this.width, this.height);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.darkGray);
        this.setTitle("Nouvelle facture - Hepthatlon GUI - ID #13");
        // make the frame visible
        this.setVisible(true);
        this.setAlwaysOnTop(true);


        //region Listeners
        annuler.addActionListener(e -> dispose());
        ajouter.addActionListener(e -> {
            // ici ajouter le produit a la liste
            tableModel.addRow(
                    new String[]{
                            String.valueOf(comboBoxStock.getSelectedItem()),
                            String.valueOf(((Produit) comboBoxStock.getSelectedItem()).getPrix()),
                            nombreSelect.getText()});
            System.out.println("Produit ajouté");
            // calcul du total
            double total = 0;
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                //reference
                Double prix = Double.parseDouble((String) tableModel.getValueAt(i, 1));
                Double quantite = Double.parseDouble((String) tableModel.getValueAt(i, 2));
                total += prix * quantite;
                //--> (String reference, int Quantite)
            }
            this.total.setText("Total : " + total + " €");
        });
        valider.addActionListener(e -> {
            /**
             *            System.out.println("Envois de la facture...");
             *             JAXBContext jaxbContext;
             *
             *             ProduitList pl = new ProduitList(new ArrayList<Produit>(){
             *                 {add(p1); add(p2);}
             *             });
             *
             *             jaxbContext = JAXBContext.newInstance(ProduitList.class);
             *             Marshaller plMarshaller = jaxbContext.createMarshaller();
             *
             *             StringWriter sw = new StringWriter();
             *             plMarshaller.marshal(pl, sw);
             *
             *             String s = client.editerFacture(sw.toString(), "CB");
             */

            ProduitList toBuy = new ProduitList();
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                int qte = Integer.parseInt((String) tableModel.getValueAt(i, 2));
                for (int j = 0; j < qte; j++) {
                    toBuy.add(new Produit((String) tableModel.getValueAt(i, 0)));
                }
                //reference
                System.out.println(tableModel.getValueAt(i, 0));                //qte
                System.out.println(tableModel.getValueAt(i, 2));
                // TODO --> (String reference, int Quantite)
                System.out.println("Mode de paiement : " + comboBoxModePaiement.getSelectedItem());

            }

            System.out.println("Envois de la facture...");
            JAXBContext jaxbContext;

            try {
                jaxbContext = JAXBContext.newInstance(ProduitList.class);
                Marshaller plMarshaller = jaxbContext.createMarshaller();
                StringWriter sw = new StringWriter();
                plMarshaller.marshal(toBuy, sw);
                String factureAPrint = client.editerFacture(sw.toString(), "CB");

                jaxbContext = JAXBContext.newInstance(Facture.class);
                Unmarshaller plUnmarshaller = jaxbContext.createUnmarshaller();
                Facture f = (Facture) plUnmarshaller.unmarshal(new StringReader(factureAPrint));

                System.out.println(factureAPrint);

            } catch (JAXBException ex) {
                ex.printStackTrace();
            }
            dispose();
        });
        //endregion
    }
}