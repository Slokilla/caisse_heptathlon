package heptathlon.view;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MainView {
    static int windowWidth = 500;
    static int windowHeight = 300;

    public static void main(String[] args) {
        // creating instance of JFrame
        JFrame f = new JFrame();

        // creating instance of JButton
        ArrayList<JButton> choix = new ArrayList<>();
        choix.add(new JButton("➕ Nouvelle facture"));
        choix.add(new JButton("🔍 Consulter stock"));
        choix.add(new JButton("📜 Chercher facture"));

        int offset = 20;
        int btnThickness = windowHeight / (choix.size() + 2);
        Rectangle rectangle = new Rectangle(
                offset,
                offset,
                windowWidth - (offset * 3),
                btnThickness);
        for (JButton btn : choix) {
            // defining location
            btn.setBounds(rectangle);
            rectangle.y += btnThickness + offset;
            // tweaking colors
            btn.setBorder(null);
            btn.setBackground(Color.GRAY);
            btn.setForeground(Color.WHITE);
            btn.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 25));
        }
        // add event listener

        // add buttons to JFrame
        for (JButton btn : choix) {
            f.add(btn);
        }

        f.setSize(windowWidth, windowHeight);
        f.setLayout(null);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.darkGray);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setTitle("Hepthatlon GUI - ID #13");
        // make the frame visible
        f.setVisible(true);

        //region Listeners
        choix.get(0).addActionListener(e -> {
            NouvelleFacture fenetreNouvelleFacture = new NouvelleFacture();
        });
        choix.get(1).addActionListener(e -> {
            ShowStock fenetreShowStock = new ShowStock();
        });
        choix.get(2).addActionListener(e -> {
            ChercherFacture fenetreChercherFacture = new ChercherFacture();
        });
        //endregion
    }
}