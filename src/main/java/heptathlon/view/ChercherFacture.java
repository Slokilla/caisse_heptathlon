package heptathlon.view;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class ChercherFacture extends JFrame {
    private final int width = 300;
    private final int height = 400;
    private final int margin = 10;
    Object[][] data;
    JLabel searchLabel;
    JTextField search;
    JTable table;
    DefaultTableModel tableModel;

    public ChercherFacture() {
        data = new Object[][]{
                {"A80020", 18},
                {"A80023", 5},
                {"C28700", 182},
        };

        searchLabel = new JLabel("N° de facture :");
        search = new JTextField("");
        int thirdW = this.width / 3;
        int twothirdW = (2 * this.width) / 3;
        searchLabel.setBounds(margin, margin, twothirdW - (margin * 3), 50 - margin);
        search.setBounds(margin + twothirdW, margin, thirdW - (margin * 3), 50 - margin);

        tableModel = new DefaultTableModel();
        table = new JTable(tableModel);
        table.setFillsViewportHeight(true);
        tableModel.addColumn("Numero");
        tableModel.addColumn("Date");
        for (Object[] row : data) {
            tableModel.addRow(row);
        }


        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(margin, margin + 50, this.width - (margin * 3), this.height - (margin * 5 + 50));

        table.setBackground(Color.darkGray);
        table.setForeground(Color.white);

        searchLabel.setBackground(Color.darkGray);
        searchLabel.setForeground(Color.white);

        table.getTableHeader().setBackground(Color.darkGray);
        table.getTableHeader().setForeground(Color.white);

        scrollPane.setBorder(null);
        scrollPane.setBackground(Color.darkGray);
        scrollPane.setForeground(Color.white);



        this.add(search);
        this.add(searchLabel);
        this.add(scrollPane);


        // Parametrage de la JFrame
        this.setSize(this.width, this.height);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.darkGray);
        this.setTitle("Chercher une facture - Hepthatlon GUI - ID #13");
        // make the frame visible
        this.setVisible(true);
        this.setAlwaysOnTop(true);


        //region Listeners
        search.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable(search.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable(search.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable(search.getText());
            }
        });
        //endregion
    }

    private void updateTable(String text_searched) {
        for (int row = 0; row < tableModel.getRowCount(); row++) {
            tableModel.removeRow(row);
        }
        // TODO ici ajouter les références trouvés en fonctions de text_searched
        tableModel.addRow(new Object[]{text_searched, 182});
    }
}