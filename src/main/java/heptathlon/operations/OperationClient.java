package heptathlon.operations;

import javax.xml.bind.JAXBContext;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class OperationClient {

    private static OperationClient instance = null;
    OperationService client;

    private OperationClient() {
        try {
            client = (OperationService) Naming.lookup(OperationService.LOOKUPNAME);
        } catch (NotBoundException | RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized OperationClient getInstance(){
        if (instance == null)
            return new OperationClient();
        return instance;
    }

    public OperationService getClient() {
        return client;
    }
}
