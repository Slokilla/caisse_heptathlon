package heptathlon;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Env {

    private static Env instance = null;
    private int numMagasin;
    private String dbName;

    private Env() {
        try {
            InputStream input = new FileInputStream("C:\\Users\\Dsimo\\Documents\\Java\\ArchitectureClientServeurM1MIAGE\\heptathlon_caisse\\src\\main\\resources\\environment.properties");
            Properties prop = new Properties();
            prop.load(input);

            numMagasin = Integer.parseInt(prop.getProperty("numMagasin"));
            dbName = prop.getProperty("dbName");


        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public int getNumMagasin() {
        return numMagasin;
    }

    public String getDbName() {
        return dbName;
    }

    public static synchronized Env getInstance() {
        if (instance == null)
            instance = new Env();
        return instance;
    }

    private void loadData() {

    }


}
