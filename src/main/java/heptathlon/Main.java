package heptathlon;

import heptathlon.POJO.Facture;
import heptathlon.POJO.Famille;
import heptathlon.POJO.Produit;
import heptathlon.operations.OperationClient;
import heptathlon.wrapper.FamilleList;
import heptathlon.operations.OperationService;
import heptathlon.wrapper.ProduitList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException, JAXBException {

        //region Commander des articles, et recevoir la facture à imprimer
        System.out.println("Tentative de contact du serveur...");
        OperationService client = OperationClient.getInstance().getClient();
        JAXBContext jaxbContext;

        String p1xml = client.getProduct(1);
        String p2xml = client.getProduct(2);

        jaxbContext = JAXBContext.newInstance(Produit.class);
        System.out.println(p1xml);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Produit p1 = (Produit) jaxbUnmarshaller.unmarshal(new StringReader(p1xml));
        Produit p2 = (Produit) jaxbUnmarshaller.unmarshal(new StringReader(p2xml));

        ProduitList pl = new ProduitList(new ArrayList<Produit>(){{add(p1); add(p2);}});

        jaxbContext = JAXBContext.newInstance(ProduitList.class);
        Marshaller plMarshaller = jaxbContext.createMarshaller();

        StringWriter sw = new StringWriter();
        plMarshaller.marshal(pl, sw);

        String s = client.editerFacture(sw.toString(), "CB");

        jaxbContext = JAXBContext.newInstance(Facture.class);
        Unmarshaller fUnmarshaller = jaxbContext.createUnmarshaller();

        Facture f = (Facture)fUnmarshaller.unmarshal(new StringReader(s));


        System.out.println(client.getFacture(f.getId()));
        //endregion

       /* try{
            String f = client.getAllProduit();

            jaxbContext = JAXBContext.newInstance(ProduitList.class);
            System.out.println(f);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ProduitList lf = (ProduitList) jaxbUnmarshaller.unmarshal(new StringReader(f));

            for(Produit fa : lf.getList()) {
                System.out.println(fa.getReference());
            }
        } catch (Exception e){
            e.printStackTrace();
        }*/
    }
}
