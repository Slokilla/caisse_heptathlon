package heptathlon.POJO;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.StringWriter;
import java.util.Objects;

@XmlRootElement
@XmlSeeAlso(Famille.class)
public class Produit {
    private int id;
    private String reference;
    private Double prix;
    private Famille famille;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }


    public Famille getFamille() {
        return famille;
    }

    public void setFamille(Famille famille) {
        this.famille = famille;
    }


    ///////CONSTRUCTORS////////


    public Produit(String reference, Double prix) {
        this.reference = reference;
        this.prix = prix;
    }

    public Produit() {
    }

    public Produit(String ref) {
        this.reference = ref;
    }


    ///////SURCHARGES/////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Produit that = (Produit) o;

        if (id != that.id) return false;
        if (!Objects.equals(reference, that.reference)) return false;
        return Objects.equals(prix, that.prix);
    }

    @Override
    public String toString() {
        return this.getReference();
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (prix != null ? prix.hashCode() : 0);
        return result;
    }
}
