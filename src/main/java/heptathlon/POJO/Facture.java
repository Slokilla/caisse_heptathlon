package heptathlon.POJO;

import heptathlon.Env;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlSeeAlso(Produit.class)
public class Facture {
    private int id;
    private String modePaiement;
    private String dateFacture;
    private Double total;
    private String designation;
    private int magasin_ID;
    private List<Produit> produits = new ArrayList<Produit>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModePaiement() {
        return modePaiement.toString();
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }


    public String getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(String dateFacture) {
        this.dateFacture = dateFacture;
    }


    public Double getTotal() {
        return total;
    }

    public void calculateTotal(){
        this.total = 0d;
        for (Produit p:produits) {
            this.total += p.getPrix();
        }
    }


    public String getDesignation() {
        return designation;
    }

    private void setDesignation(String d) {
        this.designation = d;
    }

    public void calculateDesignation(){
        this.designation = this.modePaiement
                + this.dateFacture.replaceAll("\\D", "")
                + Integer.toString(this.id);
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public void addProduit(Produit produit){
        this.produits.add(produit);
    }

    public void removeProduit(Produit produit){
        this.produits.remove(produit);
    }


    public int getMagasin_ID() {
        return magasin_ID;
    }

    public void setMagasin_ID(int id){
        this.magasin_ID = id;
    }

    ////////Constructeurs////////
    public Facture() {
        Env prop = Env.getInstance();
        magasin_ID = prop.getNumMagasin();
    }

    public Facture(ArrayList<Produit> produits, ModePaiement mp) {
        Env prop = Env.getInstance();
        magasin_ID = prop.getNumMagasin();

        this.modePaiement = mp.getMode();
        this.dateFacture = java.time.LocalDate.now().toString();
        this.calculateTotal();
        this.calculateDesignation();

    }


    ////////OVERRRIDES/////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Facture that = (Facture) o;

        if (id != that.id) return false;
        if (!Objects.equals(modePaiement, that.modePaiement)) return false;
        if (!Objects.equals(dateFacture, that.dateFacture)) return false;
        return Objects.equals(total, that.total);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (modePaiement != null ? modePaiement.hashCode() : 0);
        result = 31 * result + (dateFacture != null ? dateFacture.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }

    public static enum ModePaiement{
        CB ("CB"),
        Cheque ("Cheque"),
        Espece ("Espece");

        private String mode = "";

        ModePaiement(String name){
            this.mode = name;
        }

        public String getMode(){
            return mode;
        }
    }
}
