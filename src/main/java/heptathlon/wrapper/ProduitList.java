package heptathlon.wrapper;

import heptathlon.POJO.Produit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement()
@XmlSeeAlso(Produit.class)
public class ProduitList {

    @XmlElement(name = "Produit")
    private List<Produit> list;

    public ProduitList() {
    }

    public ProduitList(List<Produit> list) {
        this.list = list;
    }

    public List<Produit> getList() {
        return list;
    }

    public void add(Produit p){
        if(list == null)
            list = new LinkedList<>();
        list.add(p);
    }

    public Produit[] asArray() {
        Produit[] produits = new Produit[list.size()];
        for (Produit produit : this.list) {
            produits[this.list.indexOf(produit)] = produit;
        }
        return produits;
    }
}

